import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from './store/actions/action';
import LoginForm from './Components/LoginForm';
import { Button, TextField, ListItem, ListItemText } from '@material-ui/core';
import { FixedSizeList } from 'react-window';
import QuestionOne from './Components/QuestionOne';
import QuestionTwo from './Components/QuestionTwo';
import QuestionThree from './Components/QuestionThree';
import QuestionFour from './Components/QuestionFour';
import MainMenu from './Components/MainMenu';
import { BrowserRouter as Router,Switch,Route } from 'react-router-dom';


 {/* <QuestionOne/>
        <QuestionTwo/>
        <QuestionThree/>
        <QuestionFour/> */}


function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <MainMenu />
        <Switch>
          <Route path="/question1" component={QuestionOne} />
          <Route path="/question2" component={QuestionTwo} />
          <Route path="/question3" component={QuestionThree} />
          <Route path="/question4" component={QuestionFour} />
        </Switch>

      </div>
    </Router>

  );
}

export default App;
