
// import {SAVE_COUNTRY_NAME} from './actionType; '
export const SAVE_COUNTRY_NAME  = 'SAVE_COUNTRY_NAME';
export const LIST_ALL_COUNTRIES  = 'LIST_ALL_COUNTRIES';
export const CHECK_CODE_FROM_LIST = 'CHECK_CODE_FROM_LIST';
export const SAVE_USER_DETAILS = 'SAVE_USER_DETAILS';





export  const saveCountryname = (countryName)=> dispatch =>{
    // console.log(countryName,"countryname in action")
    return fetch(`api/question1/${countryName}`)
        .then(response => response.text())
        .then(data=> dispatch({type:SAVE_COUNTRY_NAME,payload:JSON.parse(data)}))
        .catch(error=>console.log(error,"error in q1"))
}

export const listAllCountries = (region)=> dispatch =>{
    console.log("region",region)
    return fetch(`https://restcountries.eu/rest/v2/region/${region}`)
    .then(response => response.text())
    .then(data => dispatch({type:LIST_ALL_COUNTRIES,payload:JSON.parse(data)}))
}

export const checkCodeFromList = (countryCode)=> dispatch=>{
    console.log(countryCode,"checkCodeFromList in action")
    return fetch(`api/question2/${countryCode}`)
            .then(response => response.text())
            .then(data => dispatch({type:CHECK_CODE_FROM_LIST,payload:JSON.parse(data)}))
}

export const saveUserDetails = (user)=> dispatch=>{
    return fetch(`api/question5/${user.name}/${user.email}/${user.password}`)
            .then(response => response.text())
            .then(data => dispatch({type:SAVE_USER_DETAILS,payload:JSON.parse(data)}))
}






