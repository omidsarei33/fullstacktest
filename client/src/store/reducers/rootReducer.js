import { SAVE_COUNTRY_NAME, LIST_ALL_COUNTRIES, CHECK_CODE_FROM_LIST,SAVE_USER_DETAILS } from '../actions/actionType';

const initialState = {
    countryName: {},
    countriesList: [],
    checkCodeList: [],
    userDetails:{}
}


function rootReducer(state = initialState, action) {
    switch (action.type) {
        case SAVE_COUNTRY_NAME:
            console.log(action, "actioncheck")
            return { ...state,countryName: action.payload }
        case LIST_ALL_COUNTRIES:
            console.log(action, "actioncheck")
            return {  ...state,countriesList: action.payload }
        case CHECK_CODE_FROM_LIST:
            console.log(action, "actioncheck")
            return  {
                ...state,
                checkCodeList: [...state.checkCodeList,...action.payload]
            }
            
        case SAVE_USER_DETAILS:
            console.log(action, "actioncheck")
            return {  ...state,userDetails: action.payload }
        default:
            return state;
    }
}

export default rootReducer;