import React from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup'
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button, TextField } from '@material-ui/core';

const LoginForm = () => {

    const dispatch = useDispatch();
    const userSaved = useSelector(state => state.userDetails)
    // console.log(userSaved.email, "userSaved")

    const LoginSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required'),
        email: Yup.string()
            .email('Invalid email')
            .required('Required'),
        password: Yup.string()
            .min(2, 'Too Short!')
            .max(20, 'Too Long!')
            .required('Required'),

    });

    return (
        <div>
            <h5>Registration FORM</h5>
            <div style={{ marginBottom: '30px' }}>
                {userSaved && userSaved.email ? `User ${userSaved && userSaved.email} Was Saved Successfully` : ""}
            </div>
            <Formik
                initialValues={{ name: '', email: "", password: "" }}
                validationSchema={LoginSchema}
                onSubmit={(values, actions) => {
                    console.log(values, "CHECKTHEVALUES")
                    dispatch(Actions.saveUserDetails(values));
                }}
            >
                {props => (
                    <Form onSubmit={props.handleSubmit}>
                        
                        <div>
                            <TextField required name="name" label="Name" onChange={props.handleChange} value={props.values.name} />
                            {props.errors.name && props.touched.name ? (<div style={{ color: 'red' }}>{props.errors.name}</div>) : null}
                        </div>
                        <div>
                            <TextField required name="email" label="email" onChange={props.handleChange} value={props.values.email} />
                            {props.errors.email && props.touched.email ? (<div style={{ color: 'red' }}>{props.errors.email}</div>) : null}
                        </div>
                        <div>
                            <TextField required name="password" label="password" onChange={props.handleChange} value={props.values.password} />
                            {props.errors.password && props.touched.password ? (<div style={{ color: 'red' }}>{props.errors.password}</div>) : null}
                        </div>
                        <Button style={{ marginTop: "30px",marginBottom:'60px' }} variant="contained" color="primary" type='submit'>Submit</Button>

                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default LoginForm;