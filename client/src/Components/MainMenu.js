import React, { useState } from 'react';
import { Button, TextField, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';



const MainMenu = () => {

    const useStyles = makeStyles({
        mainStyle: {
            width: "100%",
            margin: "auto",
            border: "0.25px solid #282c34",
            // height:"100vh"
        },
        button: {
            marginTop: "20px",
            marginBottom: '2%',
            width: "20%",
            height: "45px",
            marginLeft: "20px",
            fontSize:"10px",
            "&:hover": {
                backgroundColor: "white",
                color: "#3f51b5",
                border: "1px solid #3f51b5"
            },
            "&:active": {
                backgroundColor: "white",
                color: "#3f51b5",
                border: "1px solid #3f51b5"
            }
        },
        link:{
            textDecoration:"none",
        }



    });

    const classes = useStyles();

    return (

        <div className={classes.mainStyle}>
            <h2>Full-Stack Yobetit Test</h2>
            <div>
                <Link className={classes.link} to="/question1">
                    <Button className={classes.button} variant="contained" color="primary" >Question1</Button>
                </Link>
                <Link className={classes.link} to="/question2">
                    <Button className={classes.button} variant="contained" color="primary" >Question2</Button>
                </Link>
                <Link className={classes.link} to="/question3">
                    <Button className={classes.button} variant="contained" color="primary" >Question3</Button>
                </Link>
                <Link className={classes.link} to="/question4">
                    <Button className={classes.button} variant="contained" color="primary" >Question4</Button>
                </Link>
               
            </div>
        </div>
    )

}

export default MainMenu;