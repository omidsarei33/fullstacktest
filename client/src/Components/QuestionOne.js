import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button, TextField, ListItem, ListItemText } from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const QuestionOne = () => {

    const dispatch = useDispatch();
    const countryName = useSelector(state => state.countryName);

    const LoginSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, 'Too Short!')
            .max(50, 'Too Long!')
            .required('Required')
    });
    console.log(countryName, "countryNamein Component")

    function createData(name, region, subRegion, nationality) {
        return { name, region, subRegion, nationality };
    }

    const rows = [
        createData('Country Name', countryName && countryName.countryname),
        createData('Region', countryName && countryName.region),
        createData('Sub Region', countryName && countryName.subregion),
        createData('Nationality', countryName && countryName.nationalities),
    ];

    const useStyles = makeStyles({
        table: {
            width: "80%",
            margin: "auto",
        },
        inputfield: {
            marginTop: "20px",
            width: "50%"
        },
        imageBox: {
            height: "120px",
            backgroundColor: "#282c34",
        },
        flagStyle: {
            position: "relative",
            display: countryName && countryName.flagUrl ? "inline" : "none",
            width: "130px",
            height: "120px",
            border: '0px'
        }

    });

    const classes = useStyles();
    return (
        <div>
            
            <Formik
                initialValues={{ name: '' }}
                validationSchema={LoginSchema}
                onSubmit={(values, actions) => {
                    dispatch(Actions.saveCountryname(values.name.toLocaleLowerCase()));
                    // {countryName.region ? null : (<p>Invalid Counrty Name</p>)}
                }}
            >
                {props => (
                    <Form onSubmit={props.handleSubmit}>

                        <div style={{ width: '100%' }}>
                            <div style={{height:"20px"}}>
                                {props.errors.name && props.touched.name ? (<div style={{ color: 'red', textAlign: "left", marginLeft: "7%" }}>{props.errors.name}</div>) : null}
                            </div>
                            <TextField className={classes.inputfield} required name="name" label="Name" variant="outlined" onChange={props.handleChange} value={props.values.name} />
                            <Button style={{ marginTop: "20px", marginBottom: '60px', width: "35%", height: "55px", marginLeft: "20px" }} variant="contained" color="primary" type='submit'>Submit</Button>
                        </div>
                    </Form>
                )}
            </Formik>
            <h2>Country Detail</h2>
            <div className={classes.imageBox}>
                <img className={classes.flagStyle} src={countryName && countryName.flagUrl} />
            </div>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.name}>
                                <TableCell component="th" scope="row">
                                    {row.name}
                                </TableCell>
                                <TableCell align="right">{row.region}</TableCell>
                                <TableCell align="right">{row.subregion}</TableCell>
                                <TableCell align="right">{row.nationalities}</TableCell>
                                {/* <TableCell align="right">{row.protein}</TableCell> */}
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}

export default QuestionOne;