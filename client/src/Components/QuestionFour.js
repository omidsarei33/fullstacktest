import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button, TextField, ListItem, ListItemText } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import cherr from "../assets/cherry.jpg";
import banana from "../assets/banana.jpeg";
import lemon from "../assets/lemon.jpg";
import apple from "../assets/apple.png";

const QuestionFour = () => {

    const [apiresponse4, setApiResponse4] = useState();
    const [credit, setCredit] = useState(20);
    const dispatch = useDispatch();
    const countriesList = useSelector(state => state.countriesList);
    console.log(apiresponse4, "apiresponse4")
    const fetchQuestion4 = async () => {
        fetch(`api/question4/${credit}`)
            .then(response => response.text())
            .then(data => {
                setApiResponse4(JSON.parse(data))
                setCredit(parseInt(JSON.parse(data).balance));
            })
    }
    const firstreeltxt = apiresponse4 && apiresponse4.firstReelSpin;
    const secondreeltxt = apiresponse4 && apiresponse4.secondReelSpin;
    const thirdreeltxt = apiresponse4 && apiresponse4.thirdReelSpin;


    const reel = (reel) => {
        switch (reel) {
            case "cherry":
                return cherr
                break;
            case "banana":
                return banana
                break;
            case "lemon":
                return lemon
                break;
            case "apple":
                return apple
                break;
            default:
                break;
        }
    }

    return (
        <div>
            <div style={{ width: "100%" }}>
                <div style={{backgroundColor: "#282c34",borderRadius:"10px", color: "white",width:"100%",margin:"auto",boxShadow: "0px 0px 23px 10px rgba(0,0,40,0.31)"}}>
                    <p style={{ marginTop: "30px", height: "30px" }}>{firstreeltxt === secondreeltxt && secondreeltxt === thirdreeltxt ? "WOW 3 IN A ROW" : null}</p>
                    <div style={{ width: "100%", margin: "auto", marginTop: "30px", marginBottom: "30px" }}>
                        <img style={{ borderRadius:"20px",marginRight: "20px" }} src={reel(firstreeltxt)} height="100px" width="70px" />
                        <img style={{ borderRadius:"20px",marginRight: "20px" }} src={reel(secondreeltxt)} height="100px" width="70px" />
                        <img style={{ borderRadius:"20px",marginRight: "20px" }} src={reel(thirdreeltxt)} height="100px" width="70px" />
                    </div>
                    <div>
                        <p style={{ height: "20px", marginTop: "30px", width: "100%", margin: "auto" }}>  {apiresponse4 && apiresponse4.amountWin > 0 ? `You Won ${apiresponse4 && apiresponse4.amountWin}` : null}</p>
                        <p style={{ marginTop: "30px", width: "100%", margin: "auto" }}>The Balance is :{apiresponse4 && apiresponse4.balance}</p>
                    </div>
                    <div style={{ marginBottom: "30px" }}>
                        <Button onClick={fetchQuestion4} style={{ marginTop: "30px",marginBottom:"20px" }} variant="contained" color="primary">Spin</Button>
                    </div>
                </div>
            </div>


        </div>
    )
}

export default QuestionFour;