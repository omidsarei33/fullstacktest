import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import * as Actions from '../store/actions/action';
import { Button, TextField, ListItem, ListItemText } from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import Select from 'react-select';
import * as Yup from 'yup';
import { stat } from 'fs';

const QuestionTwo = () => {

    const dispatch = useDispatch();
    let checkCountryCode = useSelector(state => state.checkCodeList);
    console.log(checkCountryCode, "checkCountryCode component")
    const [countryArr, setCountryArr] = useState([]);
    const Array_Without_DoubleQuotes = JSON.stringify(countryArr).toString().replace(/"/g, "");
    const useStyles = makeStyles({
        table: {
            width: "80%",
            margin: "auto",
        },
        inputfield: {
            marginTop: "20px",
            width: "50%"
        },
        searchfield: {
            marginTop: "20px",
            width: "80%",
            height:"55px"

        },
        clearSection: {
            backgroundColor: "#282c34",
            color: "white",
            borderRadius: ' 20px',
            marginBottom: "20px"
        }

    });

    const customStyles = {
        control: base => ({
          ...base,
          height: 55,
          minHeight: 35
        })
      };
    const classes = useStyles();


    const handleSearch = (() => {
        countryArr && countryArr.map((item) => {
            dispatch(Actions.checkCodeFromList(item.toLocaleLowerCase()));
        })
        setCountryArr("")
    })

    const options = (() => {
        let arr = [];
        checkCountryCode && checkCountryCode.map((item) => {
            arr.push({
                value: item.name,
                label: item.name
            })
        })
        return arr;
    })

    return (
        <div style={{ height: "100vh" }}>
            <Formik
                initialValues={{ name: '' }}
                onSubmit={(values, { resetForm }, actions) => {
                    console.log(values, "CHECKTHEVALUES")
                    setCountryArr([...countryArr, values.name]);
                    resetForm();
                }}
            >
                {props => (
                    <Form onSubmit={props.handleSubmit}>

                        <div style={{ width: '100%' }}>
                            <div style={{ height: "20px" }}>
                            </div>
                            <h2>Create an list of keywords to be able to search for the countries who thier name partially match the keywords</h2>
                            <TextField className={classes.inputfield} required name="name" label="Country Name" variant="outlined" onChange={props.handleChange} value={props.values.name} />
                            <Button style={{ marginTop: "20px", marginBottom: '60px', width: "35%", height: "55px", marginLeft: "20px" }} variant="contained" color="primary" type='submit'>Add</Button>
                        </div>
                        <div>
                            <div className={classes.clearSection}>
                                <p style={{ height: "20px" }}>{countryArr && countryArr.length > 0 && Array_Without_DoubleQuotes}</p>
                                <Button style={{ marginTop: "20px", width: "30%", marginBottom: '60px', height: "55px", marginLeft: "20px" }} variant="contained" color="primary" onClick={() => { setCountryArr("") }}>Clear</Button>
                            </div>
                            <div style={{ display: "flex", width: '100%' }}>
                                <div style={{ width: "50%" }}>
                                    <Button style={{ marginTop: "20px", width: "80%", marginBottom: '60px', height: "55px", marginLeft: "20px" }} variant="contained" color="primary" onClick={handleSearch}>Find Matched Countries</Button>
                                </div>
                                <div style={{ width: '50%'}}>
                                    <Select
                                        className={classes.searchfield}
                                        placeholder="Select Country"
                                        options={options()}
                                        styles={customStyles}
                                    />
                                </div>
                            </div>

                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export default QuestionTwo;